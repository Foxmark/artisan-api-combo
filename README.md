## Artisan API Tools

This is a collection or Artisan command for Laravel. It enables easy and fast API components creation.
Using one command you can create set of generic components to speed up development process.

### Installation

- `cd` to existing laravel project. 
- run `composer require foxmark/artisan-api-combo`
- run `composer install`
- run `php artisan list` to check if you can find new `foxmark` section listing all new commands

### Usage

- for **NEW** project only start with: `php artisan foxmark:init-setup`
- use `php artisan foxmark:make-api-combo --help` to get help
- to create new API endpoint run: `php artisan foxmark:make-api-combo [BASE_NAME]`


### List of all commands

- `foxmark:init-base-request`                Create BaseApiRequest file
- `foxmark:init-base-service-interface`      Create BaseServiceInterface file
- `foxmark:init-setup`                       Creates set of basic file: BaseApiRequest and BaseServiceInterface. Note: You only need to run this once - when setting up the new project.
- `foxmark:make-api-combo`                   Creates Custom API endpoint Controller, Service, Request Model, Factory, Migration and basic set of tests.
- `foxmark:make-controller`                  Create a new Controller class
- `foxmark:make-request`                     Create a new Request class
- `foxmark:make-service`                     Create a new Service class
- `foxmark:make-service-interface`           Create a new Service Interface class
- `foxmark:make-test`                        Create a new test suite
