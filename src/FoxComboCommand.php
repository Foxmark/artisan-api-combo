<?php

namespace Foxmark\ArtisanApiCombo;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class FoxComboCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foxmark:make-api-combo {basename : Base name for all elements. eg. \'Account\' will give AccountController, AccountService, AccountRequest etc.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Custom API endpoint Controller, Service, Request Model, Factory, Migration and basic set of tests.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $basename = $this->argument('basename');

        // create model
        $params = ['--factory' => 1, '--migration' => 1, '--seed' => 1, 'name' => $basename];
        Artisan::call('make:model', $params);
        $result = Artisan::output();
        echo $result;

        // create service interface
        $params = ['name' => $basename];
        Artisan::call('foxmark:make-service-interface', $params);
        $result = Artisan::output();
        echo $result;

        // create service
        $params = ['name' => $basename];
        Artisan::call('foxmark:make-service', $params);
        $result = Artisan::output();
        echo $result;

        // create request
        $params = ['name' => $basename];
        Artisan::call('foxmark:make-request', $params);
        $result = Artisan::output();
        echo $result;

        // create controller
        $params = ['name' => $basename];
        Artisan::call('foxmark:make-controller', $params);
        $result = Artisan::output();
        echo $result;

        // create tests
        $params = ['name' => $basename];
        Artisan::call('foxmark:make-test', $params);
        $result = Artisan::output();
        echo $result;

        echo 'Make sure to bind your interface and concrete class in your AppServiceProvider file:' . PHP_EOL;
        echo '$this->app->bind('.$basename.'Interface::class, '.$basename.'Service::class);' . PHP_EOL;

        return 0;
    }
}
