<?php

namespace Foxmark\ArtisanApiCombo;

use Illuminate\Support\ServiceProvider;
use Foxmark\ArtisanApiCombo\FoxInitialSetupCommand;
use Foxmark\ArtisanApiCombo\FoxComboCommand;
use Foxmark\ArtisanApiCombo\FoxMakeControllerCommand;
use Foxmark\ArtisanApiCombo\FoxMakeRequestCommand;
use Foxmark\ArtisanApiCombo\FoxMakeServiceInterfaceCommand;
use Foxmark\ArtisanApiCombo\FoxMakeServiceCommand;
use Foxmark\ArtisanApiCombo\FoxMakeTestCommand;
use Foxmark\ArtisanApiCombo\FoxMakeBaseServiceInterfaceCommand;
use Foxmark\ArtisanApiCombo\FoxMakeBaseRequestCommand;
use Foxmark\ArtisanApiCombo\FoxMakeBaseCollectionCommand;
use Foxmark\ArtisanApiCombo\FoxMakeBaseResourceCommand;

class ArtisanApiCombo extends ServiceProvider {
    public function register()
    {
        $this->commands([
            FoxInitialSetupCommand::class,
            FoxMakeBaseRequestCommand::class,
            FoxMakeBaseServiceInterfaceCommand::class,
            FoxMakeBaseCollectionCommand::class,
            FoxMakeBaseResourceCommand::class,
            FoxComboCommand::class,
            FoxMakeControllerCommand::class,
            FoxMakeRequestCommand::class,
            FoxMakeServiceInterfaceCommand::class,
            FoxMakeServiceCommand::class,
            FoxMakeTestCommand::class
        ]);
    }
}