<?php

namespace Foxmark\ArtisanApiCombo;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class FoxInitialSetupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foxmark:init-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates set of basic file: BaseApiRequest, BaseServiceInterface, BaseResource and BaseCollection. Note: You only need to run this once - when setting up the new project.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // create service interface
        Artisan::call('foxmark:init-base-request', ['name' => 'noname']);
        $result = Artisan::output();
        echo $result;

        // create service interface
        Artisan::call('foxmark:init-base-service-interface', ['name' => 'noname']);
        $result = Artisan::output();
        echo $result;

        // create base collection
        Artisan::call('foxmark:init-base-collection', ['name' => 'noname']);
        $result = Artisan::output();
        echo $result;

        // create base resource
        Artisan::call('foxmark:init-base-resource', ['name' => 'noname']);
        $result = Artisan::output();
        echo $result;

        echo 'Setup is now complete.' . PHP_EOL;

        return 0;
    }
}
